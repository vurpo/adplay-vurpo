/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Build ALSA output */
#define DRIVER_ALSA 1

/* Build AO output */
/* #undef DRIVER_AO */

/* Build disk writer */
#define DRIVER_DISK 1

/* Build EsounD output */
/* #undef DRIVER_ESOUND */

/* Build null output */
#define DRIVER_NULL 1

/* Build OSS driver */
#define DRIVER_OSS 1

/* Build QSA driver */
/* #undef DRIVER_QSA */

/* Build SDL output */
#define DRIVER_SDL 1

/* Defined if AdPlug supports the getsubsong() method */
#define HAVE_ADPLUG_GETSUBSONG /**/

/* Defined if AdPlug supports the NukedOPL synth */
#define HAVE_ADPLUG_NUKEDOPL /**/

/* Defined if AdPlug supports the surround/harmonic synth */
#define HAVE_ADPLUG_SURROUND /**/

/* Define to 1 if you have the `dlopen' function. */
#define HAVE_DLOPEN 1

/* Define to 1 if you have the <getopt.h> header file. */
#define HAVE_GETOPT_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `adplug' library (-ladplug). */
/* #undef HAVE_LIBADPLUG */

/* Define to 1 if you have the `asound' library (-lasound). */
#define HAVE_LIBASOUND 1

/* Define to 1 if you have the `stdc++' library (-lstdc++). */
#define HAVE_LIBSTDC__ 1

/* Define to 1 if you have the "SDL.h" header file */
#define HAVE_SDL_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Name of package */
#define PACKAGE "adplay"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "adplay"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "adplay 1.8.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "adplay"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.8.1"

/* Define to 1 if all of the C90 standard headers exist (not just the ones
   required in a freestanding environment). This macro is provided for
   backward compatibility; new code need not use it. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "1.8.1"
