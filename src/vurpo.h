#pragma once

// apparently the c++ standard lib and adplay have conflicting definitions of max and min??
#undef max
#undef min
#include "output.h"
#include <unistd.h>
#include <chrono>
#include <thread>

class VurpoOutput: public Player
{
public:
    VurpoOutput(Copl *nopl): opl(nopl) {
        next_end = std::chrono::steady_clock::now();
    }

    virtual void frame() {
        std::this_thread::sleep_until(next_end);
        playing = p->update();
        float refresh = p->getrefresh();
        next_end = std::chrono::steady_clock::now() + std::chrono::microseconds((uint64_t)(1000000*(1/refresh)));

    }

    virtual Copl *get_opl() { return opl; }

private:
    Copl		*opl;
    std::chrono::steady_clock::time_point next_end;
};
